<?php

global $base_path, $base_url, $civicrm_root;

//define('CIVICRM_SITE_KEY', 'some stuff');

$db_string = 'mysql://user:pass@localhost:3306/dbname?new_link=true';

// define the database strings
define('CIVICRM_UF_DSN', $db_string);
define('CIVICRM_DSN', $db_string);

$civicrm_root = '/path/to/vendor/civicrm/civicrm-core';
define('CIVICRM_TEMPLATE_COMPILEDIR', 'sites/default/files/civicrm/templates_c/');
define('CIVICRM_UF_BASEURL', $base_url . '/');
