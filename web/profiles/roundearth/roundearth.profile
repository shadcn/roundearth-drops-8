<?php

/**
 * @file
 * Enables modules and site configuration for a Round Earth site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Entity\FilterFormat;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function roundearth_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'roundearth_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.contact_us recipient.
 */
function roundearth_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('default')->setRecipients([$site_mail])->trustData()->save();
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function roundearth_message_template_presave(EntityInterface $template) {
  // Ensure message templates have valid input formats.
  $text = $template->get('text');

  foreach ($text as $delta => $item) {
    if (!FilterFormat::load($item['format'])) {
      switch ($item['format']) {
        case 'full_html':
          $text[$delta]['format'] = 'panopoly_wysiwyg_full';
          break;

        default:
          $text[$delta]['format'] = filter_fallback_format();
      }
    }
  }

  $template->set('text', $text);
}
