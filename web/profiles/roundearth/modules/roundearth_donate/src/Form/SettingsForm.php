<?php

namespace Drupal\roundearth_donate\Form;

use CiviCRM_API3_Exception;
use Drupal\civicrm\Civicrm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * CiviCRM service.
   *
   * @var \Drupal\civicrm\Civicrm
   */
  protected $civicrm;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory service.
   * @param \Drupal\civicrm\Civicrm $civicrm
   *   CiviCRM service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Civicrm $civicrm) {
    parent::__construct($config_factory);
    $this->civicrm = $civicrm;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('civicrm')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['roundearth_donate.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'roundearth_donate_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('roundearth_donate.settings');

    $contrib_pages = $this->getContribPages();

    $form['donate_page'] = [
      '#type' => 'select',
      '#title' => $this->t('Donate page'),
      '#description' => $this->t('Select a contribution page for the "Donate" navigation link. Only "Active" pages are available.'),
      '#default_value' => $config->get('donate_page'),
      '#options' => $contrib_pages,
      '#empty_value' => '- ' . $this->t('None') . ' -',
    ];

    $form['join_page'] = [
      '#type' => 'select',
      '#title' => $this->t('Join page'),
      '#description' => $this->t('Select a contribution page for the "Join" navigation link. Only "Active" pages are available.'),
      '#default_value' => $config->get('join_page'),
      '#options' => $contrib_pages,
      '#empty_value' => '- ' . $this->t('None') . ' -',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('roundearth_donate.settings');
    $config->set('donate_page', $form_state->getValue('donate_page'));
    $config->set('join_page', $form_state->getValue('join_page'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Gets a list of contribution pages.
   *
   * @return array
   *   Array of contribution page titles, keyed by ID.
   */
  protected function getContribPages() {
    $this->civicrm->initialize();

    try {
      $result = civicrm_api3('ContributionPage', 'get');
    }
    catch (CiviCRM_API3_Exception $e) {
      return [];
    }

    $pages = [];

    foreach ($result['values'] as $page) {
      $title = $page['title'];
      if (empty($page['is_active'])) {
        $title .= ' (' . $this->t('Inactive') . ')';
      }
      $pages[$page['id']] = $title;
    }

    return $pages;
  }

}
