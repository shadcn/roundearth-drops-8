<?php

namespace Drupal\roundearth_migration\CiviCRM;

/**
 * CiviCRM Custom fields service.
 */
class CustomFields {

  use CiviCrmAwareTrait;

  protected $fields = [];
  protected $select = [];

  public function getField($name) {
    $this->getCivi()->initialize();

    if (!isset($this->fields[$name])) {
      try {
        $this->fields[$name] = civicrm_api3('CustomField', 'getsingle', ['name' => $name]);
      }
      catch (\Exception $e) {
        $this->fields[$name] = FALSE;
      }

    }

    return $this->fields[$name];
  }

  public function getSelectValue(array $field, $label) {
    $this->getCivi()->initialize();

    $key = $field['id'] . '|' . $label;

    if (isset($this->select[$key])) {
      return $this->select[$key];
    }

    // Direct match on value.
    try {
      $params = [
        'option_group_id' => $field['option_group_id'],
        'value' => $label,
      ];
      $result = civicrm_api3('OptionValue', 'getsingle', $params);
      return $this->select[$key] = $result['value'];
    }
    catch (\Exception $e) {
      // No op.
    }

    // Direct match on label.
    try {
      $params = [
        'option_group_id' => $field['option_group_id'],
        'label' => $label,
      ];
      $result = civicrm_api3('OptionValue', 'getsingle', $params);
      return $this->select[$key] = $result['value'];
    }
    catch (\Exception $e) {
      // No op.
    }

    return $this->select[$key] = FALSE;
  }

}
