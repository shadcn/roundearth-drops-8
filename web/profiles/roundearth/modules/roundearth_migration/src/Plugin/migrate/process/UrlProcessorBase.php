<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use DOMElement;
use Drupal\Component\Utility\Html;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * A base class for migration processors that modify link or image URLs.
 */
abstract class UrlProcessorBase extends ProcessPluginBase {

  use LoggingTrait;

  /**
   * Controls whether or not to process URLs in image tags.
   *
   * @var bool
   */
  protected $processImages = TRUE;

  /**
   * Controls whether or not to process URLs in link tags.
   *
   * @var bool
   */
  protected $processLinks = TRUE;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $this->migrateExecutable = $migrate_executable;
    $changed = FALSE;
    $document = Html::load($value);

    // Find links.
    if ($this->processLinks) {
      /** @var \DOMElement[] $links */
      $links = iterator_to_array($document->getElementsByTagName('a'));
      foreach ($links as $link) {
        $href = $link->getAttribute('href');
        if ($href && ($url = $this->processUrl($href))) {
          $this->setUrl($link, 'href', $url, $href);
          $changed = TRUE;
        }
        else {
          $this->deferSetUrl($link, $href);
        }
      }
    }

    // Find images.
    if ($this->processImages) {
      /** @var \DOMElement[] $images */
      $images = iterator_to_array($document->getElementsByTagName('img'));
      foreach ($images as $image) {
        $src = $image->getAttribute('src');
        if ($src && ($url = $this->processUrl($src))) {
          $this->setUrl($image, 'src', $url, $src);
          $changed = TRUE;
        }
        else {
          $this->deferSetUrl($image, $src);
        }
      }
    }

    if (!$changed) {
      return $value;
    }

    $text = Html::serialize($document);
    return $text;
  }

  /**
   * Set the URL onto a DOM element's attribute.
   *
   * @param \DOMElement $element
   *   The DOM element.
   * @param string $attribute
   *   The attribute name.
   * @param string $value
   *   The value to set for the attribute.
   * @param string $original
   *   The original value for the attribute.
   */
  protected function setUrl(DOMElement $element, $attribute, $value, $original) {
    $element->setAttribute($attribute, $value);
  }

  /**
   * Called when URL is not changed.
   *
   * This allows subclasses to log information or take other actions as needed.
   *
   * @param \DOMElement $element
   *   The DOM element.
   * @param string $value
   *   The value to set for the attribute.
   */
  protected function deferSetUrl(DOMElement $element, $value) {
    // No op.
  }

  /**
   * Process the URL.
   *
   * @param string $url
   *   The link.
   *
   * @return string|false
   *   The processed link, or FALSE if it was determined that the link should
   *   not be changed.
   */
  abstract protected function processUrl($url);

  /**
   * Gets an internal URL.
   *
   * @see \Drupal\roundearth_migration\InternalUrl::get
   */
  protected function internalUrl($url, $hosts = NULL, $validSchemes = ['http', 'https']) {
    return $this->getInternalUrl()->get($url, $hosts, $validSchemes);
  }

  /**
   * Gets the internal URL service.
   *
   * @return \Drupal\roundearth_migration\InternalUrl
   *   The internal URL service.
   */
  protected function getInternalUrl() {
    return \Drupal::service('roundearth_migration.internal_url');
  }

}
