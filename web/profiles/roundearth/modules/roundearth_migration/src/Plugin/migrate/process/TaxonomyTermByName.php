<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaxonomyTermByName.
 *
 * Use "name" (source value) and optionally "vid" plugin configuration options
 * to load a taxonomy term.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_taxonomy_term_by_name"
 * )
 */
class TaxonomyTermByName extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TaxonomyTermByName.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $params = ['name' => trim($value)];
    if ($this->configuration['vid']) {
      $params['vid'] = $this->configuration['vid'];
    }

    if ($terms = $this->getTaxonomyTermStorage()->loadByProperties($params)) {
      return reset($terms)->id();
    }

    // Create a term if configured to do so.
    if (!empty($this->configuration['create'])) {
      $term = $this->getTaxonomyTermStorage()->create([
        'name' => $this->configuration[$value],
        'vid' => $this->configuration['vid'],
      ]);
      $term->save();
      return $term->id();
    }

    // Can't find one, this stinks.
    if ($this->configuration['vid']) {
      throw new MigrateSkipProcessException(sprintf('Unable to find term "%s" in vid "%s"', $value, $this->configuration['vid']));
    }
    throw new MigrateSkipProcessException(sprintf('Unable to find term "%s"', $value));
  }

  /**
   * Gets the taxonomy term storage service.
   *
   * @return \Drupal\taxonomy\TermStorageInterface
   *   The taxonomy term storage service.
   */
  protected function getTaxonomyTermStorage() {
    return $this->entityTypeManager->getStorage('taxonomy_term');
  }

}
