<?php

namespace Drupal\roundearth_migration\Plugin\migrate\source\d6;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d6\Node as NodeD6;

/**
 * Custom D6 node source.
 *
 * Replaces the class for the 'd6_node' plugin.
 */
class Node extends NodeD6 {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'alias' => $this->t('Path alias'),
    ] + parent::fields();
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');

    // Include path alias.
    if ($alias = $this->getAlias($nid)) {
      $row->setSourceProperty('alias', '/' . $alias);
    }

    // Include taxonomy data.
    if ($this->moduleExists('taxonomy')) {
      $row->setSourceProperty('taxonomy', $this->getTaxonomy($nid));
    }

    return parent::prepareRow($row);
  }

  /**
   * Gets a node's alias.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return string|null
   *   The alias, or NULL if not found.
   */
  protected function getAlias($nid) {
    $query = $this->select('url_alias', 'ua')
      ->fields('ua', ['dst']);
    $query->condition('ua.src', 'node/' . $nid);
    return $query->execute()->fetchField();
  }

  /**
   * Gets a node's taxonomy data.
   */
  protected function getTaxonomy($nid) {
    if (!$this->moduleExists('taxonomy')) {
      return NULL;
    }

    $query = $this->select('node', 'n')->fields('t', ['tid', 'vid']);
    $query->innerJoin('term_node', 'tn', 'n.vid = tn.vid');
    $query->innerJoin('term_data', 't', 't.tid = tn.tid');
    $query->condition('n.nid', $nid);
    return $query->execute()->fetchAll();
  }

}
