/**
 * @file
 * Photo grid implementation.
 */
(function ($, Drupal) {

  Drupal.behaviors.photoGrid = {
    attach: function (context) {
      $('.photo-grid [data-toggle="lightbox"]', context).on('click', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox({
          alwaysShowClose: true,
        });
      });
    }
  };

})(jQuery, Drupal);
