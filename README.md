RoundEarth
==========

RoundEarth is a template for building a website for a nonprofit organization,
based on Drupal 8 and CiviCRM.

It allows organizations with limited budgets to get started with something
that's "most of the way there" and continue to improve, rather than starting
from scratch.

TODO: talk up the features (aka value) that organizations will get from it

