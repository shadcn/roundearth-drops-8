<?php

namespace Civi\Postal;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class Services {

  public static function registerServices(ContainerBuilder $container) {
    // Add our services.
    $container->setDefinition('postal.signature_verifier', new Definition(\Civi\Postal\SignatureVerifier::class));
    $container->setDefinition('postal.api', new Definition(\Civi\Postal\PostalAPI::class));
    $container->setDefinition('postal.flexmailer_send_listener', new Definition(\Civi\Postal\FlexMailerSendListener::class))
      ->setArguments([ new Reference('postal.api') ]);
    $container->setDefinition('postal.civimail_webhook_listener', new Definition(\Civi\Postal\CiviMailPostalWebhookListener::class))
      ->setArguments([ new Reference('postal.api') ]);

    // Add our event listeners.
    $container->findDefinition('dispatcher')
      ->addMethodCall('addListener', [\Civi\FlexMailer\FlexMailer::EVENT_SEND,
        [new Reference('postal.flexmailer_send_listener'), 'processEvent']])
      ->addMethodCall('addListener', [\Civi\Postal\PostalEvent::NAME,
        [new Reference('postal.civimail_webhook_listener'), 'processEvent']]);
  }

}
