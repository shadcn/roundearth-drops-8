<?php

namespace Civi\Postal;

/**
 * Listen for Postal webhooks and do stuff for CiviMail.
 */
class CiviMailPostalWebhookListener {

  /**
   * @var \Civi\Postal\PostalAPI
   */
  protected $api;

  /**
   * Constructs a CiviMailPostalWebhookListener.
   *
   * @param \Civi\Postal\PostalAPI $api
   *   The Postal API service.
   */
  public function __construct(PostalAPI $api) {
    $this->api = $api;
  }

  /**
   * Process a Postal event.
   *
   * @param \Civi\Postal\PostalEvent $event
   */
  public function processEvent(PostalEvent $event) {
    $payload = $event->getPayload();
    $client = $this->api->getClient();

    try {
      // Get the message headers.
      $message = $client->makeRequest('messages', 'message', [
        'id' => $payload['message']['id'],
        '_expansions' => ['headers'],
      ]);
    }
    catch (\Exception $e) {
      \Civi::log()->error('Unable to lookup message headers via Postal API: {exception}', ['exception' => $e]);
      return;
    }

    // Extract CiviMail info from the message headers.
    $civimail_info = NULL;
    if (isset($message->headers->{'x-civimail-bounce'})) {
      $civimail_info = $this->parseCiviMailBounceAddress($message->headers->{'x-civimail-bounce'}[0]);
    }
    if (!$civimail_info) {
      // This doesn't appear to be a message from CiviMail. Skip it!
      return;
    }

    switch ($event->getName()) {
      case 'MessageDeliveryFailed':
      case 'MessageBounce':
        \civicrm_api3('Mailing', 'event_bounce', [
          'job_id' => $civimail_info['job_id'],
          'event_queue_id' => $civimail_info['event_queue_id'],
          'hash' => $civimail_info['hash'],
          'body' => !empty($payload['details']) ? $payload['details'] : 'Bounced',
          // @todo Is it possible to reuse the same processing CiviMail does?
          // @todo Maybe check out 'BouncePattern'?
          'bounce_type_id' => !empty($payload['status']) && $payload['status'] == 'HardFail' ? 6 : 11,
        ]);
        break;

      // @todo Re-enable once we're sure we won't double record opens from CiviMail
      //case 'MessageLoaded':
      //  \CRM_Mailing_Event_BAO_Opened::open($civimail_info['event_queue_id']);
      //  break;

      case 'MessageLinkClicked':
        // @todo See https://github.com/imba-us/com.imba.sendgrid/blob/master/webhook.php#L142
        break;

    }
  }

  /**
   * Simplistic parsing of the X-CiviMail-Bounce header.
   *
   * @param string $address
   *   The bounce address.
   * @return array|null
   *   An array with the ids parsed out of the address or NULL if invalid.
   */
  protected function parseCiviMailBounceAddress($address) {
    list ($front,) = explode('@', $address);
    if (strpos($front, '+') !== FALSE) {
      list (, $front) = explode('+', $front);
    }
    $parts = explode('.', $front);
    if (count($parts) < 4) {
      return NULL;
    }

    return [
      'job_id' => $parts[1],
      'event_queue_id' => $parts[2],
      'hash' => $parts[3],
    ];
  }

}