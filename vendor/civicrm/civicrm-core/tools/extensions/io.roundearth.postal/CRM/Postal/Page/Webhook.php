<?php

use Civi\Postal\PostalEvent;
use CRM_Postal_ExtensionUtil as E;

class CRM_Postal_Page_Webhook extends CRM_Core_Page {

  public function run() {
    $body = file_get_contents('php://input');

    $verify = \Civi::settings()->get('postal_webhook_verify_signature');
    if ($verify) {
      $signature = $_SERVER['HTTP_X_POSTAL_SIGNATURE'];
      /** @var \Civi\Postal\SignatureVerifier $signature_verifier */
      $signature_verifier = \Civi::service('postal.signature_verifier');
      $dkim_string = \Civi::settings()->get('postal_default_dkim_record');

      if (!$signature_verifier->verify($body, $signature, $dkim_string)) {
        CRM_Core_Error::debug('Postal_Webhook_Signature_Check_Failed', [
          'signature' => $signature,
          'dkim_string' => $dkim_string,
          'body' => $body,
        ], TRUE, FALSE);

        http_response_code(400);
        CRM_Utils_System::civiExit();
      }
    }

    $data = json_decode($body, TRUE);
    if (empty($data['event']) || empty($data['payload'])) {
        CRM_Core_Error::debug('Postal_Webhook_Bad_Payload', $data, TRUE, FALSE);

        http_response_code(400);
        CRM_Utils_System::civiExit();
    }

    $event = new PostalEvent($data['event'], $data['payload']);
    \Civi::dispatcher()->dispatch(PostalEvent::NAME, $event);

    CRM_Utils_System::civiExit();
  }

}
